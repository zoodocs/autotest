from django.contrib import admin
from test_api import models

# Register your models here.


class TestApiAdmin(admin.ModelAdmin):
    list_display =  ['api_name', 'api_method', 'api_url', 'api_body', 'api_response', 'create_time']


class TestCasesAdmin(admin.ModelAdmin):
    list_display = ['testcase_name', 'testcase_desc', 'response', 'result', 'create_time']


admin.site.register(models.TestApi, TestApiAdmin)
admin.site.register(models.TestCases, TestCasesAdmin)
