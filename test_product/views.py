from django.shortcuts import render
from test_product import models

# Create your views here.


# 产品管理
def product_manage(request):
    username = request.session.get('user', '')
    product_lists = models.Product.objects.all()
    return render(request, 'testProduct/product_mange.html', {'products': product_lists})
